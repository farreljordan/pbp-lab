from django.http import response
from django.shortcuts import render
from lab_2.models import Note

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'notes':notes}
    return render(request, 'lab5_index.html', response)
