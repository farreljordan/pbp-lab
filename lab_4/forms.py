from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(NoteForm, self).__init__(*args, **kwargs)
        self.fields['to'].widget.attrs = {'class': 'form-control'}
        self.fields['fromm'].widget.attrs = {'class': 'form-control'}
        self.fields['title'].widget.attrs = {'class': 'form-control'}
        self.fields['message'].widget.attrs = {'class': 'form-control'}
    class Meta:
        model = Note
        fields = [
            'to', 
            'fromm', 
            'title',
            'message'
        ]
        labels  = {
        'fromm':'From'
        }