from django import forms
from lab_1.models import Friend


class FriendForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FriendForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs = {'class': 'form-control'}
        self.fields['npm'].widget.attrs = {'class': 'form-control'}
        self.fields['birth_date'].widget.attrs = {'class': 'form-control'}
    class Meta:
        model = Friend
        fields = [
            'name', 
            'npm', 
            'birth_date'
        ]
    

