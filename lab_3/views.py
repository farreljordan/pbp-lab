from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, "lab3_index.html", response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    
    form = FriendForm(request.POST or None)
    if (request.method == "POST" and form.is_valid()):
        form.save()
        return HttpResponseRedirect('/lab-3/')

    response = {'form':form}
    return render(request, 'lab3_form.html', response)