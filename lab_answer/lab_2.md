1. Apakah perbedaan antara JSON dan XML?
   Secara sintaks JSON dan XML memiliki perbedaan yang terlihat jelas. JSON sendiri memiliki sintaks yang lebih simpel dibandingkan XML dimana JSON memiliki tag awal dan akhir dikarenakan json didasarkan pada bahasa pemrograman JavaScript. JSON juga mendukung tipe data seperti string, number, array dan lain-lain sedangkan XML hanya berupa string. Selain itu, JSON juga merupakan sebuah format data, sedangkan XML merupakan markup language.

2. Apakah perbedaan antara HTML dan XML?
   Pada dasarnya sendiri XML dan HTML meiliki perbedaan tujuan, dimana XML bertujuan untuk transfer informasi sedangkan HTML untuk penyajian data. Selain itu, secara sintaks pun juga berbeda XML case sensitive dan XML membaca whitespaces, sedangkan HTML case insensitive dan tidak membaca whitespaces.
